import { ActionTree } from "vuex";
import BannerState from "../../types/CountryState";
import rootStore from "@vue-storefront/core/store";
import * as types from "./mutation-types";
import { Logger } from "@vue-storefront/core/lib/logger";
import { processURLAddress } from "@vue-storefront/core/helpers";
import { adjustMultistoreApiUrl } from "@vue-storefront/core/lib/multistore";
import i18n from "@vue-storefront/i18n";
const axios = require("axios");
//console.log('alBannerEntityName', alBannerEntityName)
const actions: ActionTree<BannerState, any> = {
  async Countrylist(context) {
    try {
      debugger;
      let url = rootStore.state.config.countrylist.endpoint;
      console.log("url:", url)
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url);
        console.log("url adjustMultistoreApiUrl:", url)
      }
      Logger.info(
        "Magento 2 REST API Request with Request Data",
        "get-country"
      )();
      await fetch(url,{
        method: "GET",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json"
        }
      })
        .then(response => response.json())
        // .then(data=> console.log("api data", data))
        // .catch(err => console.log(err, "error"));
        .then(data => {
          if (data && data.code === 200) {
            Logger.info("Magento 2 REST API Response Data", "get-country", {
              data
            })();
            if (data && data.code === 200) {
              context.commit(types.COUNTRY_FETCH_COUNTRY, data.result);
            } else {
            //   rootStore.dispatch("notification/spawnNotification", {
            //     type: "error",
            //     message: data.result.message,
            //     action1: { label: i18n.t("OK") }
            //   });
            }
          } else {
            Logger.error(
              "Something went wrong. Try again in a few seconds.",
              "get-country"
            )();
          }
        });
    } catch (e) {
      console.log(e, "error");
      Logger.error(
        "Something went wrong. Try again in a few seconds.",
        "get-country"
      )();
    }
  }
};
export default actions;
