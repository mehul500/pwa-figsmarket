import { MutationTree } from 'vuex'
import * as types from './mutation-types'
export const mutations: MutationTree<any> = {
  [types.COUNTRY_FETCH_COUNTRY] (state, countries) {
    state.countries = countries || []
  }
}
