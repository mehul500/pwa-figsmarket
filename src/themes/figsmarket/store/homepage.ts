import { prepareQuery } from '@vue-storefront/core/modules/catalog/queries/common'

export const homepageStore = {
  namespaced: true,
  state: {
    new_collection: [],
    bestsellers: [],
    banner_data:[],
  },
  actions: {
    async fetchNewCollection({ commit, dispatch }) {
      // debugger;
      const newProductsQuery = prepareQuery({ queryConfig: 'newProducts' })
      const newProductsResult = await dispatch('product/list', {
        query: newProductsQuery,
        size: 8,
        sort: 'created_at:desc'
      }, { root: true })
      const configuredProducts = await dispatch(
      'category-next/configureProducts',
       { products: newProductsResult.items
        }, { root: true })
      commit('SET_NEW_COLLECTION', configuredProducts)
    },
    async loadBestsellers ({ commit, dispatch }) {
      const response = await dispatch('product/list', {
        query: prepareQuery({ queryConfig: 'bestSellers' }),
        size: 8,
        sort: 'created_at:desc'
      }, { root: true })
      commit('SET_BESTSELLERS', response.items)
    },
    // async loadBannerdata ({ commit, dispatch }) {
    //   const response = await dispatch('product/list', {
    //     query: prepareQuery({ queryConfig: 'aureate_banner' }),
    //     size: 8,
    //     sort: 'created_at:desc'
    //   }, { root: true })

    //   commit('SET_BANNER_DATA', response.items)
    // }
  },
  mutations: {
    SET_NEW_COLLECTION (state, products) {
      state.new_collection = products || []
    },
    SET_BESTSELLERS (state, bestsellers) {
      state.bestsellers = bestsellers
    },
    SET_BANNER_DATA (state, bannerdata) {
      state.banner_data = bannerdata
    }
  },
  getters: {
    getEverythingNewCollection (state) {
      console.log("check banner data :", state)
      return state.new_collection
    },
    getBestsellers (state) {
           return state.bestsellers
    }
  }
}
