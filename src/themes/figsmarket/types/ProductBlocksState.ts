export default interface ProductBlocksState {
  productBlocks: any[],
  currentStaticBlock: any[]
}
