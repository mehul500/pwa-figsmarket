export default interface MeasurementState {
  measurements: any[],
  currentMeasurements: {
    productId: number,
    unit: string,
    measurements: any[],
    designerComment: string
  },
  customerMeasurements: {
    unit: string,
    measurements: any[]
  }
}
